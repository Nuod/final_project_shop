package com.shop.Service;

import com.shop.DAO.IUserRepository;
import com.shop.DTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class UserService implements IUserService {
    private final static Logger logger = Logger.getLogger(UserService.class.getName());
    @Autowired
    private IUserRepository userRepository;

    @Override
    public User getUserById(String login) {
        return userRepository.getUserByLogin(login);
    }

    @Override
    public void changeBan(String login) {
        User user = userRepository.getUserByLogin(login);
        if (user.getBan() != 0) {
            user.setBan(0);
            userRepository.update(user);
        } else {
            user.setBan(1);
            userRepository.update(user);
        }
    }

    @Override
    public void regUser(User user) {
        userRepository.create(user);
    }

    @Override
    public User checkUser(User user) {
        User currentUser = userRepository.getUserByLogin(user.getLogin());
        if (currentUser != null && user.getLogin().equals(currentUser.getLogin()) && user.getPassword().equals(currentUser.getPassword())) {
            return currentUser;
        } else {
            return null;
        }
    }

    @Override
    public Boolean checkUniqueLogin(String login) {
        if (userRepository.getUserByLogin(login) == null){
            return true;
        } else {
            return false;
        }
    }
    @Override
    public List<User> getAllUser(){
        return userRepository.getAllUsers();
    }
}
