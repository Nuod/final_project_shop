package com.shop.Service;

import com.shop.DAO.IProductRepository;
import com.shop.DTO.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;


@Service
public class ProductService implements IProductService {

    private final static Logger logger = Logger.getLogger(ProductService.class.getName());
    @Autowired
    private IProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public void addProduct(Product product) {
        productRepository.create(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.delete(productId);
    }

    @Override
    public void updateProduct(Product product) {
        productRepository.update(product);
    }

    @Override
    public Product getProductById(Long productId){
        return productRepository.getProductById(productId);
    }

    @Override
    public List<Product> getProductsByPathOfName(String pathName){
        return productRepository.getProductByPathOfName(pathName);
    }

}
