package com.shop.Service;

import com.shop.DTO.Product;

import java.util.List;

public interface IProductService {
    List<Product> getAllProducts();

    void addProduct(Product product);

    void deleteProduct(Long productId);

    void updateProduct(Product product);

    Product getProductById(Long productId);

    List<Product> getProductsByPathOfName(String pathName);
}
