package com.shop.Service;

import com.shop.DTO.Product;
import com.shop.DTO.Sale;
import com.shop.DTO.SaleView;

import java.util.List;

public interface ISaleService {

    Sale getActivSaleByUserLogin(String sale);


    void deleteProductFromSale(Sale sale, Long productId);

    public void deleteSale(Long saleId);

    Double getTotalPriceOfSale(Sale sale);

    void updateSale(Sale sale);

    List<SaleView> convertItemListforView(Sale sale);



}
