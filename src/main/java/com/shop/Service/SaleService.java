package com.shop.Service;

import com.shop.comparator.ItemComparator;
import com.shop.DAO.IProductRepository;
import com.shop.DAO.ISaleRepository;
import com.shop.DTO.Item;
import com.shop.DTO.Product;
import com.shop.DTO.Sale;
import com.shop.DTO.SaleView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class SaleService implements ISaleService {

    private final static Logger logger = Logger.getLogger(ProductService.class.getName());
    @Autowired
    private ISaleRepository saleRepository;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IItemService iItemService;

    @Override
    public Sale getActivSaleByUserLogin(String login) {
        return saleRepository.getSaleByUserLogin(login);
    }


    @Override
    public void deleteProductFromSale(Sale sale, Long productId) {
        iItemService.zeroItem(sale.getId(),productId);
    }

    @Override
    public void deleteSale(Long saleId) {
    saleRepository.delete(saleId);
    }

    @Override
    public Double getTotalPriceOfSale(Sale sale) {
        List<SaleView> items = this.convertItemListforView(sale);
        double sum = 0.0;
        for (SaleView itr: items) {
            sum +=itr.getPrice()*itr.getCount();
        }
        return sum;
    }

    @Override
    public void updateSale(Sale sale){
        saleRepository.update(sale);
    }

    @Override
    public List<SaleView> convertItemListforView(Sale sale) {
        List<Item> items = sale.getItemList();
        if (items == null || items.isEmpty()) {
            return null;
        }
        items.sort(new ItemComparator());
        List<SaleView> res = new ArrayList<>();
        int count = 1;
        long productId = items.get(0).getProductId();
        for (int i = 1; i < items.size(); i++) {
            if (items.get(i).getProductId() == productId) {
                count++;
            } else {
                Product product = productRepository.getProductById(productId);
                res.add(new SaleView(product.getId(), product.getName(), product.getPrice() * count, count));
                count = 1;
                productId = items.get(i).getProductId();
            }
        }
        Product product = productRepository.getProductById(productId);
        res.add(new SaleView(product.getId(), product.getName(), product.getPrice() * count, count));
        return res;
    }
}
