package com.shop.Service;

import com.shop.DAO.IItemRepository;
import com.shop.DTO.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class ItemService implements IItemService {

    private final static Logger logger = Logger.getLogger(ItemService.class.getName());
    @Autowired
    private IItemRepository itemRepository;

    @Override
    public List<Item> getItemBySaleId(Long saleId) {
        return itemRepository.getItemBySaleId(saleId);
    }

    @Override
    public List<Item> getItemByProductId(Long productId) {
        return itemRepository.getItemByProductId(productId);
    }

    @Override
    public List<Item> getItemBySaleProductId(Long saleId, Long productId) {
        return itemRepository.getItemBySaleProductId(saleId,productId);
    }

    @Override
    public void zeroItem(Long saleId, Long productId) {
    itemRepository.zeroItem(saleId,productId);
    }

    @Override
    public void addItem(Long saleId, Long productId, Integer count) {
        itemRepository.zeroItem(saleId,productId);
        itemRepository.addItem(saleId,productId,count);
    }
}
