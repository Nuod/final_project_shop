package com.shop.Service;

import com.shop.DTO.User;

import java.util.List;

public interface IUserService {

    User getUserById(String login);

    void changeBan(String login);

    void regUser(User user);

    User checkUser(User user);

    Boolean checkUniqueLogin(String login);

    List<User> getAllUser();
}
