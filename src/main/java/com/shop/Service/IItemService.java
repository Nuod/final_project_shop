package com.shop.Service;

import com.shop.DTO.Item;

import java.util.List;

public interface IItemService {

    List<Item> getItemBySaleId(Long saleId);

    List<Item> getItemByProductId(Long ProductId);

    List<Item> getItemBySaleProductId(Long saleId, Long productId);

    void zeroItem(Long saleId, Long productId);

    void addItem(Long saleId, Long productId, Integer count);
}
