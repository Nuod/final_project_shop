package com.shop.DTO;

import java.util.List;

public class Sale {
    private long id;
    private String userLogin;
    private String statusOfSale;
    private List<Item> itemInSale;

    public Sale() {}

    public Sale(long id, String userLogin, String statusOfSale, List<Item> itemInSale) {
        this.id = id;
        this.userLogin = userLogin;
        this.statusOfSale = statusOfSale;
        this.itemInSale = itemInSale;
    }

    public Sale(String userLogin, String statusOfSale, List<Item> itemInSale) {
        this.userLogin = userLogin;
        this.statusOfSale = statusOfSale;
        this.itemInSale = itemInSale;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser() {
        return userLogin;
    }

    public void setUser(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getStatusOfSale() {
        return statusOfSale;
    }

    public void setStatusOfSale(String statusOfSale) {
        this.statusOfSale = statusOfSale;
    }

    public List<Item> getItemList() {
        return itemInSale;
    }

    public void setItemList(List<Item> itemInSale) {
        this.itemInSale = itemInSale;
    }
}
