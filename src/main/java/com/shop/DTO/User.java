package com.shop.DTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;

public class User {
    @NotEmpty(message = "Логин должен быть заполнен")
    @Size(min = 3, max = 15, message = "Логин должен быть длиннее 3 и короче 16")
    private String login;
    private String name;
    @Email(message = "Не email")
    private String email;
    private String password;
    private String role;
    private int ban;

    public User() { }

    public User(String login, String name, String email, String password, String role, int ban) {
        this.login = login;
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.ban = ban;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getBan() {
        return ban;
    }

    public void setBan(int ban) {
        this.ban = ban;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return  login.equals(user.login) &&
                name.equals(user.name) &&
                email.equals(user.email) &&
                Objects.equals(password, user.password) &&
                role == user.role &&
                Objects.equals(ban, user.ban);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, name, email, role);
    }

    @Override
    public String toString() {
        return "User" +
                "\nlogin= " + login +
                "\nname= " + name  +
                "\nemail= " + email +
                "\nrole=" + role +
                "\nban=" + ban;
    }
}
