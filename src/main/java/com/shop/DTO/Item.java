package com.shop.DTO;

public class Item {
    private Long productId;
    private Long saleId;
    private String serialNumb;

    public Item() { }

    public Item(Long productId, Long saleId, String serialNumb) {
        this.productId = productId;
        this.saleId = saleId;
        this.serialNumb = serialNumb;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public String getSerialNumb() {
        return serialNumb;
    }

    public void setSerialNumb(String serialNumb) {
        this.serialNumb = serialNumb;
    }
}
