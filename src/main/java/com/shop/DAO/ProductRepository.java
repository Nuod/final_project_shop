package com.shop.DAO;

import com.shop.DTO.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Logger;

@Repository
public class ProductRepository implements IProductRepository {

    private final static Logger logger = Logger.getLogger(ProductRepository.class.getName());
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Product> RowMap = (rowStr, rowNum) -> new Product(
            rowStr.getLong("product_id"),
            rowStr.getString("name"),
            rowStr.getString("description"),
            rowStr.getDouble("price"),
            rowStr.getString("image_url")
    );

    @Override
    public void create(Product product) {
String sql = "INSERT INTO `mydb`.`product` (`price`, `name`, `description`, `image_url`) "+
        "VALUES (?, ?, ?, ?);";
jdbcTemplate.update(sql, new Object[]{
        product.getPrice(),
        product.getName(),
        product.getDescription(),
        product.getImgUrl()
});
    }

    @Override
    public Product getProductById(Long id) {
        String sql = "SELECT * FROM product WHERE product_id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, RowMap).get(0);
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM product WHERE product_id = ?";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    @Override
    public void update(Product product) {
        String sql = "UPDATE product SET price = ?, name = ?, description = ?, image_url = ? WHERE product_id = ?";
        jdbcTemplate.update(sql, new Object[]{
                product.getPrice(),
                product.getName(),
                product.getDescription(),
                product.getImgUrl(),
                product.getId()
        });
    }

    @Override
    public List<Product> getAllProducts() {
        String sql = "SELECT * FROM product";
        return jdbcTemplate.query(sql, RowMap);
    }

    @Override
    public List<Product> getProductByPathOfName(String pathName) {
        pathName = "%" + pathName + "%";
        String sql = "SELECT * FROM product WHERE name LIKE ?";
        return jdbcTemplate.query(sql, new Object[]{pathName}, RowMap);
    }
}
