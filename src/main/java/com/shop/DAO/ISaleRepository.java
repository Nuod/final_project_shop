package com.shop.DAO;

import com.shop.DTO.Sale;

import java.util.List;

public interface ISaleRepository {

    void create(Sale sale);

    void delete(Long id);

    void update(Sale sale);

    void updatePaidStatus(Long id, String status);

    Sale getSaleById(Long id);

    Sale getSaleByUserLogin(String login);

    List<Sale> getAllSale();
}
