package com.shop.DAO;

import com.shop.DTO.User;

import java.util.List;

public interface IUserRepository {

    void create(User user);

    void update(User user);

    void updateUserRole(String login, String role);

    void delete(String login);

    User getUserByLogin(String login);

    List<User> getAllUsers();

    List<User> getAllUsersWhithBan();

}
