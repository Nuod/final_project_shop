package com.shop.DAO;

import com.shop.DTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Logger;

@Repository
public class UserRepository implements IUserRepository {

    private final static Logger logger = Logger.getLogger(UserRepository.class.getName());
    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<User> RowMap = (rowStr, rowNum) -> new User(
            rowStr.getNString("login"),
            rowStr.getString("name"),
            rowStr.getString("email"),
            rowStr.getString("password"),
            rowStr.getString("role"),
            rowStr.getInt("ban")
    );

    @Override
    public void create(User user) {
        String sql = "INSERT INTO `mydb`.`user` (`login`, `name`, `email`, `password`, `role`, `ban`) VALUES (?, ?, ?, ?,'user', 0)";;
        jdbcTemplate.update(sql, new Object[]{
                user.getLogin(),
                user.getName(),
                user.getEmail(),
                user.getPassword()
        });
    }

    @Override
    public void updateUserRole(String login, String role) {
        String sql = "UPDATE user SET role = ? where login = ?";
        jdbcTemplate.update(sql, new Object[]{
                role,
                login
        });
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE user SET name = ?, email = ?, password = ?, role = ?, ban = ? where login = ?";
        jdbcTemplate.update(sql, new Object[]{
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                user.getRole(),
                user.getBan(),
                user.getLogin()
        });
    }

    @Override
    public void delete(String login) {
        String sql = "DELETE user from user where login = ?";
        jdbcTemplate.update(sql, new Object[]{login});
    }

    @Override
    public User getUserByLogin(String login) {
        String sql = "SELECT * FROM user where login = ?";
        List<User> users = jdbcTemplate.query(sql, new Object[]{login}, RowMap);
        if(users.isEmpty()){
            return null;
        } else {
        return users.get(0);
        }
    }

    @Override
    public List<User> getAllUsers() {
        String sql = "SELECT * FROM user";
        return jdbcTemplate.query(sql, RowMap);
    }

    @Override
    public List<User> getAllUsersWhithBan() {
        String sql = "SELECT * FROM user where endban < CURDATE()";
        return jdbcTemplate.query(sql, RowMap);
    }
}

