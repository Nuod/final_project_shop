package com.shop.DAO;

import com.shop.DTO.Item;

import java.util.List;


public interface IItemRepository {
    void create(Item item);

    List<Item> getItemBySaleId(Long saleId);

    List<Item> getItemByProductId(Long productId);

    List<Item> getItemBySaleProductId(Long saleId, Long productId);

    void zeroItem(Long saleId, Long productId);

    void addItem(Long saleId, Long productId, Integer count);

    List<Item> getAllItems();
}
