package com.shop.DAO;

import com.shop.DTO.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Logger;

@Repository
public class ItemRepository implements IItemRepository {

    private final static Logger logger = Logger.getLogger(ItemRepository.class.getName());
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Item> RowMap = (rowStr, rowNum) -> new Item(
            rowStr.getLong("product_id"),
            rowStr.getLong("sale_id"),
            rowStr.getString("serial_numb")
    );

    private  RowMapper<Long> LongRowMap = (rowStr, rowNum) -> new Long(rowStr.getLong("item_id"));

    @Override
    public void create(Item item) {
        String sql =" INSERT INTO `mydb`.`Item` (`product_id`, `sale_id`, `serial_numb`) VALUES (?, NULL, ?)";
        jdbcTemplate.update(sql, new Object[]{
                item.getProductId(),
                item.getSerialNumb()
        });
    }

    @Override
    public List<Item> getItemBySaleId(Long saleId) {
        String sql = "SELECT * FROM item, sold_item WHERE sold_item.item_id=item.item_id and sold_item.sale_id = ?";
        return jdbcTemplate.query(sql, new Object[]{saleId}, RowMap);
    }

    @Override
    public List<Item> getItemByProductId(Long productId) {
        String sql = "SELECT * FROM item WHERE product_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{productId}, RowMap);
    }

    @Override
    public List<Item> getItemBySaleProductId(Long saleId, Long productId) {
        String sql = "SELECT * FROM item WHERE sale_id = ? AND product_id = ?";
        return jdbcTemplate.query(sql, new Object[]{saleId, productId}, RowMap);
    }

    @Override
    public void zeroItem(Long saleId, Long productId) {
        String sql ="DELETE FROM sold_item where sale_id = ? AND item_id IN (SELECT item_id FROM item WHERE product_id = ?)";
        //String sql = "UPDATE sold_item SET sale_id = NULL where sale_id = ? AND product_id = ?";
        jdbcTemplate.update(sql, new Object[]{saleId, productId});
    }

    @Override
    public void addItem(Long saleId, Long productId, Integer count) {
        String sql = "INSERT INTO `mydb`.`sold_item` (`sale_id`, `item_id`) VALUES (?, ?);" ;
                String sql1 = "select item.item_id from sold_item right join item on sold_item.item_id = item.item_id where sold_item.sale_id is NULL and item.product_id  = ?;";
       List<Long> freeId = jdbcTemplate.query(sql1, new Object[]{productId}, LongRowMap);
        //String sql = "UPDATE item SET sale_id = ? where sale_id is NULL AND product_id = ? LIMIT ?";
        for (int i = 0;i<count && i<freeId.size();i++){
            jdbcTemplate.update(sql, new Object[]{saleId, freeId.get(i)});
        }
        }

    @Override
    public List<Item> getAllItems() {
        String sql = "SELECT * FROM item";
        return jdbcTemplate.query(sql, RowMap);
    }
}
