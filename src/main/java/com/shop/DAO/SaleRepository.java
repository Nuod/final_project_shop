package com.shop.DAO;

import com.shop.DTO.Item;
import com.shop.DTO.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class SaleRepository implements ISaleRepository {

    private final static Logger logger = Logger.getLogger(SaleRepository.class.getName());
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IItemRepository itemRepository;

    private RowMapper<Sale> RowMap = (rowStr, rowNum) -> new Sale(
            rowStr.getLong("sale_id"),
            rowStr.getString("user_login"),
            rowStr.getString("status"),
            itemRepository.getItemBySaleId(rowStr.getLong("sale_id"))
    );

    @Override
    public void create(Sale sale) {
        String sql = "INSERT INTO `mydb`.`sale` (`user_login`, `date`, `status`) VALUES (?, '2010-10-1', 'not paid');"; //todo delete date
        jdbcTemplate.update(sql, new Object[]{sale.getUser()});
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM sale WHERE sale_id = ?";
        jdbcTemplate.update(sql, new Object[]{id});

    }

    @Override
    public void update(Sale sale) {
        String sql = "UPDATE sale SET user_login = ?, status = ? WHERE sale_id = ?";
        jdbcTemplate.update(sql, new Object[]{
                sale.getUser(),
                sale.getStatusOfSale(),
                sale.getId()
        });
    }

    @Override
    public void updatePaidStatus(Long id, String status) {
        String sql = "UPDATE sale SET status = ? WHERE sale_id = ?";
        jdbcTemplate.update(sql, new Object[]{status, id});
    }

    @Override
    public Sale getSaleByUserLogin(String login) {
        String sql = "SELECT * FROM sale WHERE user_login = ? AND status ='not paid'";
        List<Sale> sales = jdbcTemplate.query(sql, new Object[]{login}, RowMap);
        if (sales.isEmpty()) {
            this.create(new Sale(login, "no paid", null));
            return this.getSaleByUserLogin(login);
        } else {
            return sales.get(0);
        }
    }

    @Override
    public Sale getSaleById(Long id) {
        return null;
    }

    @Override
    public List<Sale> getAllSale() {
        return null;
    }
}
