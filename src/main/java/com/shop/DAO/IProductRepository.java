package com.shop.DAO;

import com.shop.DTO.Product;

import java.util.List;

public interface IProductRepository {
    void create(Product product);

    Product getProductById(Long id);

    void delete(Long id);

    void update(Product product);

    List<Product> getAllProducts();

    List<Product> getProductByPathOfName(String pathName);
}
