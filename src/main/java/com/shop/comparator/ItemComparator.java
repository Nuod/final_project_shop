package com.shop.comparator;

import com.shop.DTO.Item;

import java.util.Comparator;

public class ItemComparator implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
        if (o1.getProductId() == o2.getProductId()) {
            return 0;
        } else if (o1.getProductId() > o2.getProductId()) {
            return 1;
        } else {
            return -1;
        }
    }
}
