package com.shop.controller;

import com.shop.DTO.User;
import com.shop.Service.IUserService;
import com.shop.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

@Controller
public class UserController {
    private final static Logger logger = Logger.getLogger(UserController.class.getName());
    @Autowired
    private UserManager userManager;
    @Autowired
    private IUserService userService;

    @RequestMapping("/main")
    public ModelAndView mainPage() {
        User currentUser = userManager.getUser();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("main");
        modelAndView.addObject("user", currentUser);
        return modelAndView;
    }

    @RequestMapping("/reguser")
    public ModelAndView regUserPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("reguser");
        modelAndView.addObject("users", userService.getAllUser());
        return modelAndView;
    }
    @RequestMapping("/ban/{userLogin}")
    public ModelAndView bunUser(@PathVariable(name = "userLogin") String userLogin){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/reguser");
        userService.changeBan(userLogin);
        return modelAndView;
    }
}
