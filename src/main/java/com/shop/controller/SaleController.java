package com.shop.controller;

import com.shop.DTO.Sale;
import com.shop.DTO.User;
import com.shop.Service.IItemService;
import com.shop.Service.ISaleService;
import com.shop.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

@Controller
public class SaleController {
    private final static Logger logger = Logger.getLogger(SaleController.class.getName());
    @Autowired
    private UserManager userManager;
    @Autowired
    private ISaleService saleService;
    @Autowired
    private IItemService itemService;

    @GetMapping("/cart")
    public ModelAndView cartPage() {
        User currentUser = userManager.getUser();
        ModelAndView modelAndView = new ModelAndView();
        Sale sale = saleService.getActivSaleByUserLogin(currentUser.getLogin());
        modelAndView.setViewName("cart");
        if (currentUser.getBan() == 1){
            modelAndView.addObject("errorMsg", "Вы заблокированы! Покупка невозможна!");
        }
        modelAndView.addObject("user", currentUser);
        modelAndView.addObject("productsList", saleService.convertItemListforView(sale));

        return modelAndView;
    }

    @PostMapping("/cart")
    public ModelAndView cart() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("cart");
        return modelAndView;
    }

    @RequestMapping("/buy")
    public ModelAndView buy() {
        User currentUser = userManager.getUser();
        ModelAndView modelAndView = new ModelAndView();
        if (currentUser.getBan() == 1){
            modelAndView.setViewName("redirect:/cart");
            return modelAndView;
        }
        Sale sale = saleService.getActivSaleByUserLogin(currentUser.getLogin());
        sale.setStatusOfSale("paid");
        saleService.updateSale(sale);
        modelAndView.setViewName("redirect:/main");
        return modelAndView;
    }

    @RequestMapping("/product/buy/{productId}")
    public ModelAndView addToSale(@PathVariable(name = "productId") Long productId, @RequestParam int count) {
        ModelAndView modelAndView = new ModelAndView();
        User currentUser = userManager.getUser();
        itemService.addItem(
                saleService.getActivSaleByUserLogin(currentUser.getLogin()).getId(),
                productId,
                count
        );
        modelAndView.setViewName("redirect:/products");

        return modelAndView;
    }

    @RequestMapping("/cart/delete/{productId}")
    public ModelAndView deleteFromCart(@PathVariable(name = "productId") Long productId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/cart");
        User currentUser = userManager.getUser();
        saleService.deleteProductFromSale(
                saleService.getActivSaleByUserLogin(currentUser.getLogin()), productId);
        return modelAndView;
    }
}
