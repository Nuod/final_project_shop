package com.shop.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

@ControllerAdvice
public class ExceptionHandlerController {

    private final static Logger logger = Logger.getLogger(ExceptionHandlerController.class.getName());
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public String handleRuntimeException(Exception ex) {
        return "Что-то пошло не так, обратитесь в службу тех поддержки";
    }

}
