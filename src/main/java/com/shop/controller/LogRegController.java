package com.shop.controller;

import com.shop.DTO.User;
import com.shop.Service.IUserService;
import com.shop.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

@Controller
public class LogRegController {

    private final static Logger logger = Logger.getLogger(LogRegController.class.getName());
    @Autowired
    private UserManager userManager;
    @Autowired
    private IUserService userService;

    @GetMapping("/login")
    public ModelAndView getLoginPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/login");

        request.getSession().invalidate();

        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView login(User user) {
        ModelAndView modelAndView = new ModelAndView();
        User currentUser = userService.checkUser(user);
        if (currentUser != null) {
            userManager.setUser(currentUser);
            modelAndView.setViewName("redirect:/main");
            //modelAndView.addObject("user",currentUser);
        } else{
            modelAndView.setViewName("login");
            modelAndView.addObject("errorMsg", "Логин/Пароль неверен");
        }
            return modelAndView;
    }

    @GetMapping("/registration")
    public ModelAndView registrationPage(User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("registration");
        return modelAndView;

    }

    @PostMapping("/registration")
    public ModelAndView registration(@Validated User user, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();

        if (result.hasErrors()) {
            modelAndView.addObject("errors", result.getAllErrors());
        } else if (userService.checkUniqueLogin(user.getLogin())){
            userService.regUser(user);
            userManager.setUser(userService.getUserById(user.getLogin()));
            modelAndView.setViewName("redirect:/main");
        } else {
            modelAndView.addObject("errorMsg", "Пользователь с таким логином уже существует");
        }

        return modelAndView;
    }
}
