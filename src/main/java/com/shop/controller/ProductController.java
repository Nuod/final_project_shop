package com.shop.controller;

import com.shop.DTO.Product;
import com.shop.DTO.User;
import com.shop.Service.IProductService;
import com.shop.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

@Controller
public class ProductController {

    private final static Logger logger = Logger.getLogger(ProductController.class.getName());
    @Autowired
    private IProductService productService;
    @Autowired
    private UserManager userManager;

    @GetMapping("/products")
    public ModelAndView showProducts() {
        User currentUser = userManager.getUser();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("products");
        modelAndView.addObject("user", currentUser);
        modelAndView.addObject("Products", productService.getAllProducts());
        return modelAndView;
    }

    @RequestMapping("/product/{productId}")
    public ModelAndView moreInfoAboutProduct(@PathVariable(name = "productId") Long productId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("thing");
        modelAndView.addObject("user", userManager.getUser());
        modelAndView.addObject("prod", productService.getProductById(productId));
        return modelAndView;
    }


    @GetMapping("/products/edit/add")
    public ModelAndView addProduct() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editthing");
        modelAndView.addObject("prod", new Product());
        return modelAndView;
    }

    @PostMapping("/product/edit/save")
    public ModelAndView saveProduct(Product product) {
        ModelAndView modelAndView = new ModelAndView();
        if (product.getId() == 0) {
            productService.addProduct(product);
        } else {
            productService.updateProduct(product);
        }
        modelAndView.setViewName("redirect:/products");
        return modelAndView;
    }

    @GetMapping("/products/edit/{productId}")
    public ModelAndView editProduct(@PathVariable(name = "productId") Long productId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editthing");
        modelAndView.addObject("prod", productService.getProductById(productId));
        return modelAndView;
    }

    @RequestMapping("/product/edit/delete/{productId}")
    public ModelAndView deleteProduct(@PathVariable(name = "productId") Long productId) {
        ModelAndView modelAndView = new ModelAndView();
        productService.deleteProduct(productId);
        modelAndView.setViewName("redirect:/products");
        return modelAndView;
    }

    @RequestMapping("/products/search")
    public ModelAndView searchProducts(String searchProduct) {
        ModelAndView modelAndView = new ModelAndView();
        User currentUser = userManager.getUser();
        modelAndView.setViewName("products");
        modelAndView.addObject("Products", productService.getProductsByPathOfName(searchProduct));
        return modelAndView;

    }
}
