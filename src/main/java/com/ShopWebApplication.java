package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class ShopWebApplication {

    private final static Logger logger = Logger.getLogger(ShopWebApplication.class.getName());
    public static void main(String[] args) {
        SpringApplication.run(ShopWebApplication.class, args);


    }
}
