<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
  <title>(╯°益°)╯彡┻━┻</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/Common.css">

</head>

<div class="header">
  <h1>Кривые программы</h1>

</div>

<div class="topnav">
  <a href="/main">Главная</a>
  <a href="/products">Каталог</a>

  <c:if test="${user.role == 'admin'}">
    <a href="/reguser">Зарег. Поль.</a>
  </c:if>

  <a href="/logout" style="float:right">Выход</a>
  <a href="/cart" style="float:right">Корзина</a>
</div>

<div class="row">
  <div class="column side">

    <div class="card">
      <h2>Важная информация</h2>
      
      <p>Пре альфа версия сайта. Много чего не работает.</p>
      <p> Как вы сюда вошли?</p>
    </div>
  </div>
  <div class="column middle">
    <div class="card">
      <form action="/product/edit/save" method="POST">
        <input style="display: none" name="id" value="${prod.id}">
        <label>Название: </label>
        <input type="text" name="name" value="${prod.name}"><br>
        <label>Описание</label>
        <input type="text" name="description" value="${prod.description}"><br>
        <label>Цена</label>
        <input type="number" name="price" value="${prod.price}"><br>
        <label>Картинка</label>
        <input type="text" value="${prod.imgUrl}"><br>
        <input type="submit" value="Сохранить">
      </form>
    </div>
  </div>
</div>
<div class="column side">
  <div class="card">
    <h3>Поиск каталогу</h3>
    <div>
      <form action="/products/search" method="GET">
        <input type="text" name="searchProduct">
      <input type="submit" value="Поиск">
  </div>
  </div>
</div>
</div>
</div>
</div>


<div class="footer">
  <h2>Не связывайтесь с нами! Мы Знаем Мишу!</h2>
</div>

</html>