<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>(╯°益°)╯彡┻━┻</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/loginreg.css">

</head>
<body>
    <div class="center">
            <form action="/registration" method="POST">
                <div class="field">Login*:  <input type="text" name="login" required></div>
                <div class="field">Name*:   <input type="text" name="name" required></div>
                <div class="field">Password*:  <input type="password" name="password" required></div>
                <div class="field">Email*: <input type="text" name="email"></div>
            <div><input type="submit" value="Вход"></div></form>
            <label>${errorMsg}</label>
            <c:forEach var="error" items="${errors}">
			${error.defaultMessage}<br>
            </c:forEach>

    </div>
</body>
</html> 