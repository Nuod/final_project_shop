<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
  <title>(╯°益°)╯彡┻━┻</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/Common.css">

</head>

<div class="header">
  <h1>Кривые программы</h1>

</div>

<div class="topnav">
  <a href="/main">Главная</a>
  <a href="/products">Каталог</a>

  <c:if test="${user.role == 'admin'}">
    <a href="/reguser">Зарег. Поль.</a>
  </c:if>

  <a href="/logout" style="float:right">Выход</a>
  <a href="/cart" style="float:right">Корзина</a>
</div>

<div class="row">
  <div class="column side">
    <div class="card">
      <h2>Важная информация</h2>
      
      <p>Пре альфа версия сайта. Много чего не работает.</p>
      <p> Как вы сюда вошли?</p>
    </div>
    <div class="card">
      Сортировать по: <div class="dropdown">
        <button class="dropbtn">Выбрать</button>
        <div class="dropdown-content">
          <a href="/Products.html?SortMethod=ascending">По возрастанию</a>
          <a href="/Products.html?SortMethod=decrease">По убыванию</a>
          <a href="/Products.html?SortMethod=AZ">А-Я</a>
          <a href="/Products.html?SortMethod=ZA">Я-А</a>
        </div>
      </div>
    </div>
  </div>
  <div class="column middle">
    <div class="grid-container">
      <c:forEach var="prod" items="${Products}">
        <div class="card">
          <h2>${prod.name}</h2>
          <img class="product" src="prod.img">
          <h3>Описание</h3>
          <div>${prod.description}</div>
          <a href="/product/${prod.id}">Подробнее</a>
        </div>
      </c:forEach>
    </div>

  </div>
  <div class="column side">
    <div class="card">
      <h3>Поиск каталогу</h3>
      <div>
        <form action="/products/search" method="GET">
          <input type="text" name="searchProduct" value="${searchName}">
        <input type="submit" value="Поиск">
    </div>
    </div>
    <c:if test="${user.role == 'admin'}">
      <div class="card">
        <a href="/products/edit/add">Добавить продукт</a>
      </div>
    </c:if>
  </div>
</div>
</div>
</div>
<div class="footer">
  <h2>Footer</h2>
</div>

</html>