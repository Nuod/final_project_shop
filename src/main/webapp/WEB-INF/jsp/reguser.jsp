<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
  <title>(╯°益°)╯彡┻━┻</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/Common.css">

</head>

<div class="header">
  <h1>Кривые программы</h1>

</div>

<div class="topnav">
    <a href="/main">Главная</a>
    <a href="/products">Каталог</a>

    <c:if test="${user.role == admin}" >
      <a href="/regUser">Зарег. Поль.</a>
    </c:if>
    
    <a href="/logOut" style="float:right">Выход</a>
    <a href="/cart" style="float:right">Корзина</a>
  </div>

<div class="row">
  <div class="column side">

    <div class="card">
      <h2>Важная информация</h2>
      
      <p>Пре альфа версия сайта. Много чего не работает.</p>
      <p> Как вы сюда вошли?</p>
    </div>
  </div>
  <div class="column middle">
    <div class="card">
      <tr>
      <c:forEach var="user" items="${users}">
        <tb>
          <label>${user.login}</label>
          <c:if test="${user.ban == 1}">
            <label style="color : red">Заблок.</label>
            <form action="/ban/${user.login}"> <input type="submit" value="Разблок."></form>
          </c:if>
          <c:if test="${user.ban == 0}">
              <label style="color : green" >Разблок.</label>
              <form action="/ban/${user.login}"> <input type="submit" value="Заблок."></form>
            </c:if>
        </tb>
      </c:forEach>
    </tr>
    </div>
  </div>
  <div class="column side">
    <div class="card">
      <h3>Поиск каталогу</h3>
      <div>
        <form action="/products/search" method="GET">
          <input type="text" name="searchProduct">
        <input type="submit" value="Поиск">
    </div>
    </div>

  </div>
</div>
</div>
</div>


<div class="footer">
  <h2>Не связывайтесь с нами! Мы Знаем Мишу!</h2>
</div>

</html>