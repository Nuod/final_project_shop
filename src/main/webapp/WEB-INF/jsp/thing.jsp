<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
  <title>(╯°益°)╯彡┻━┻</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/Common.css">

</head>

<div class="header">
  <h1>Кривые программы</h1>

</div>

<div class="topnav">
    <a href="/main">Главная</a>
    <a href="/products">Каталог</a>

    <c:if test="${user.role == 'admin'}" >
      <a href="/regUser">Зарег. Поль.</a>
    </c:if>
    
    <a href="/logout" style="float:right">Выход</a>
    <a href="/cart" style="float:right">Корзина</a>
  </div>

<div class="row">
  <div class="column side">

    <div class="card">
      <h2>Важная информация</h2>
      
      <p>Пре альфа версия сайта. Много чего не работает.</p>
      <p> Как вы сюда вошли?</p>
    </div>
  </div>
  <div class="column middle">
    <div class="card">
      <c:if test="${user.role == 'admin'}">
        <p>
          <a href="/products/edit/${prod.id}">Изменить</a>
          <a href="/product/edit/delete/${prod.id}">Удалить</a>
        </p>
      </c:if>
      <h2>"${prod.name}"</h2>
      <img class="product" src="${prod.imgUrl}">
      <p>Цена: ${prod.price}</p>
      <h3>Описание</h3>
      <div>${prod.description}</div>
      <div>
        <form action="/product/buy/${prod.id}">
        <input type="number" name="count" value="1">
        <input type="submit" value="Добавить в корзину">
        </form>
      </div>
    </div>
  </div>
  <div class="column side">
    <div class="card">
      <h3>Поиск каталогу</h3>
      <div>
        <form action="/products/search" method="GET">
          <input type="text" name="searchProduct">
        <input type="submit" value="Поиск">
    </div>
    </div>

  </div>
</div>
</div>
</div>


<div class="footer">
  <h2>Не связывайтесь с нами! Мы Знаем Мишу!</h2>
</div>

</html>