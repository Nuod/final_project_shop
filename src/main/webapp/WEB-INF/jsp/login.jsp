<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Вход (╯°益°)╯彡┻━┻</title>
    <link href="../css/loginreg.css" rel="stylesheet" type="text/css">

</head>
<body>
    <div class="center">
    
            <form action="/login" method="POST">

                <div class="field"><label>Логин:</label>  <input type="text" name="Login" required></div>
                <div class="field"><label>Пароль:</label>  <input type="password" name="Password" required></div>
                <div><input type="submit" value="Вход"></div>
                </form>
            <a style="float:right" href="/registration">Регистрация</a>
            <p>${errorMsg}</p>
    </div>
</body>
</html> 