SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- ************************************** `user`

CREATE TABLE IF NOT EXISTS `mydb`.`user`
(
 `login`    varchar(16) NOT NULL ,
 `name`     varchar(45) NOT NULL ,
 `email`    varchar(128) NOT NULL ,
 `password` varchar(32) NOT NULL ,
 `role`     varchar(45) NOT NULL ,
 `ban`      int NOT NULL ,
PRIMARY KEY (`login`)
);


-- ************************************** `product`

CREATE TABLE IF NOT EXISTS `mydb`.`product`
(
 `product_id`  int NOT NULL AUTO_INCREMENT ,
 `price`       double NOT NULL ,
 `name`        varchar(45) NOT NULL ,
 `description` varchar(512) NULL ,
 `image_url`   varchar(128) NULL ,
PRIMARY KEY (`product_id`)
)ENGINE = InnoDB;


-- ************************************** `sale`

CREATE TABLE IF NOT EXISTS `mydb`.`sale`
(
 `sale_id` int NOT NULL AUTO_INCREMENT,
 `date`     date NOT NULL ,
 `status`   varchar(45) NOT NULL ,
 `user_login`    varchar(16) NOT NULL ,
PRIMARY KEY (`sale_id`),
KEY `fkIdx_27` (`user_login`),
CONSTRAINT `FK_27` FOREIGN KEY `fkIdx_27` (`user_login`) REFERENCES `mydb`.`user` (`login`)
)ENGINE = InnoDB;






-- ************************************** `item`

CREATE TABLE IF NOT EXISTS `mydb`.`item`
(
 `item_id`     int NOT NULL AUTO_INCREMENT ,
 `serial_numb` varchar(45) NOT NULL ,
 `product_id`  int NOT NULL ,
PRIMARY KEY (`item_id`),
KEY `fkIdx_40` (`product_id`),
CONSTRAINT `FK_40` FOREIGN KEY `fkIdx_40` (`product_id`) REFERENCES `mydb`.`product` (`product_id`)
)ENGINE = InnoDB;


-- ************************************** `sold_item`

CREATE TABLE IF NOT EXISTS `mydb`.`sold_item`
(
 `sale_id` int NOT NULL ,
 `item_id`  int NOT NULL ,
PRIMARY KEY(`sale_id`, `item_id`),
KEY `fkIdx_34` (`sale_id`),
CONSTRAINT `FK_34` FOREIGN KEY `fkIdx_34` (`sale_id`) REFERENCES `mydb`.`sale` (`sale_id`),
KEY `fkIdx_37` (`item_id`),
CONSTRAINT `FK_37` FOREIGN KEY `fkIdx_37` (`item_id`) REFERENCES `mydb`.`item` (`item_id`)
);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`user` (`login`, `name`, `email`, `password`, `role`, `ban`) VALUES ('root', 'dev', 'dog@dog.com', 'admin', 'admin', 0);
INSERT INTO `mydb`.`user` (`login`, `name`, `email`, `password`, `role`, `ban`) VALUES ('hacker', 'anon', 'fsociety@fk.hl', '1234', 'user', 1);
INSERT INTO `mydb`.`user` (`login`, `name`, `email`, `password`, `role`, `ban`) VALUES ('ComUs', 'Andrey', 'andrey1337@nindx.kov', 'qwer', 'user', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`product`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`product` (`product_id`, `price`, `name`, `description`, `image_url`) VALUES (1, '100', 'Antivirus', 'super puper virus killer', NULL);
INSERT INTO `mydb`.`product` (`product_id`, `price`, `name`, `description`, `image_url`) VALUES (2, '130', 'Hack Tool', 'fsociety', NULL);
INSERT INTO `mydb`.`product` (`product_id`, `price`, `name`, `description`, `image_url`) VALUES (3, '9999', 'Office ', 'base software for your office', NULL);
INSERT INTO `mydb`.`product` (`product_id`, `price`, `name`, `description`, `image_url`) VALUES (4, '1', 'Notepad', 'nothing to say', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`sales`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`sale` (`sale_id`, `user_login`, `date`, `status`) VALUES (1, 'root', '2010-10-1', 'not paid');

COMMIT;

-- -----------------------------------------------------
-- Data for table `mydb`.`item`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (1, '54g6srg4drg654d');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (1, 'e65f4sa65f1ase5');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (1, '5sef1esf3ef1eef5');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (1, '35vc1ecc5e8rg4f');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (1, 'nm65muh6m11mu');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (2, 'ds65fs6df4s65ef4');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (2, '65a1e1sfsef1se5f');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (3, 'se5f61s6ef1cee61');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (3, 'idthspohj4pj54894');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (3, 'jskvnnirrijrllkjsgslkg');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (4, 'skdlnfvwineiornvkn');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (4, 'lsefl9485h539p5hg');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (4, '894w5ooriej585u9');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (4, 'p9e48uutp9e85tuij');
INSERT INTO `mydb`.`item` (`product_id`, `serial_numb`) VALUES (4, '7w435twoytwu4ith');

COMMIT;








